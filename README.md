# Somewhere behind the stars

A space shooter who tells Alex's story, a boy who will have to save his girlfriend from pirates.

##

Site: https://simx72.codeberg.page/somewhere-behind-the-stars/

## Features

- Basic phaser scenes
- [TypeScript](https://www.typescriptlang.org/) support
- [Vite](https://vitejs.dev/) as development and build tool
- [eslint](https://eslint.org/) and [prettier](https://prettier.io/) to format code

## Use with node.js

Have [node.js](https://nodejs.org) with npm installed.

1. Clone or download this repository
2. Install dependencies `npm ci`

`npm run dev` launches the vite development server on localhost with hot reloading.

`npm run build` builds the application to `dist`.

`npm run serve` locally preview production build.

## Use with docker

Have [docker](https://www.docker.com/) with [docker-compose](https://docs.docker.com/compose/install/) installed.

`docker-compose up` will build from `dev.Dockerfile` and start the vite development server with hot reloading.

## Project Structure

```
├── dist                 // production build
├── public/assets        // static files like images, fonts or sounds
├── src                  // TypeScript files
│  ├── scenes
│  │   ├── GameScene.ts  // gameplay
│  │   ├── LoadScene.ts  // boot game, load assets
│  │   ├── MenuScene.ts  // main menu
│  │   └── UiScene.ts    // runs on top of GameScene
│  ├── game.ts           // Phaser setup
│  └── style.css         // styles for html and canvas
└── index.html           // entry point
```

## Licence

[AGPL-v3](/LICENSE)


## Credits

### Spaceship sprites extracted from `/public/assets/img/ship/shipsheetparts.png`: 
Stephen Challener (Redshrike), hosted by OpenGameArt.org

### Assets in `/public/assets/exewin-spacegame`:
CC-BY 3.0, exewin https://github.com/exewin – https://exewin.github.io/

### Boxy Bold Font
Public Domain, by Clint Bellanger https://clintbellanger.net/

### rogueFont
CC-BY-SA 4.0, The FontStruction “8x8 square roguelike ASCII font”
https://fontstruct.com/fontstructions/show/2367286 by “Deadlywere” is
licensed under a Creative Commons Attribution Share Alike license
http://creativecommons.org/licenses/by-sa/4.0/.
