/*
 * Somewhere behind the stars
 * 
 * Copyright (C) 2024  Simx72 <adventure5200@duck.com>
 * Copyright (c) 2022 pixel-fabian
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import { Asteroid } from "../objects/Asteroid";
import { Bullet } from "../objects/Bullet";
import { Ship } from "../objects/Ship";
import { ParentScene } from "./ParentScene";

export class GameScene extends ParentScene {

  //////////////////////////////////////////////////
  // LIFECYCLE (init, preload, create, update)    //
  //////////////////////////////////////////////////

  create(): void {
    super.create()
    this.ships = <any>this.add.group();
    this.bullets = <any>this.add.group();
    this.asteroids = <any>this.add.group();
  }


  //////////////////////////////////////////////////
  // Public methods                               //
  //////////////////////////////////////////////////

  ships!: PhaserGroup<Ship>;
  bullets!: PhaserGroup<Bullet>;
  asteroids!: PhaserGroup<Asteroid>;

  paused = false;

  pauseGame() {
    this.physics.pause();
    return this.paused = true;
  }

  resumeGame() {
    this.physics.resume();
    return this.paused = false;
  }

  togglePause() {
    if (this.paused) {
      this.resumeGame()
    } else {
      this.pauseGame()
    }
  }

  centerBodyOnBody(a: Phaser.Physics.Arcade.Body, b: Phaser.Physics.Arcade.Body) {
    a.position.set(
      b.x + b.halfWidth - a.halfWidth,
      b.y + b.halfHeight - a.halfHeight
    );
  }

  centerBodyOnPoint(a: Phaser.Physics.Arcade.Body, p: Phaser.Types.Math.Vector2Like) {
    if (p.x && p.y) {
      this.centerBodyOnXY(a, p.x, p.y);
    }
  }

  centerBodyOnXY(a: Phaser.Physics.Arcade.Body, x: number, y: number) {
    a.position.set(
      x - a.halfWidth,
      y - a.halfHeight
    );
  }

  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////
}
