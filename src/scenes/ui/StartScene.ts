/*
 * Somewhere behind the stars
 * 
 * Copyright (C) 2024  Simx72 <adventure5200@duck.com>
 * Copyright (c) 2022 pixel-fabian
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import { SCENES, TEXTURES } from "../../constants";
import SpriteButton from "../../objects/SpriteButton";
import {UiScene} from "../UiScene";
import { Presentation } from "../../objects/Presentation";

export class StartScene extends UiScene {
	constructor() {
		super({
			key: SCENES.START
		})
	}

	title!: Phaser.GameObjects.Text;
	slogan!: Phaser.GameObjects.Text;
	playButton!: SpriteButton;
	presentation!: Presentation;

	lineNumber = 8;

	create(): void {
		super.create()

		this.presentation = new Presentation(this, [
			{ texture: TEXTURES.BACKGROUND_1 },
			{ texture: TEXTURES.BACKGROUND_2 },
			{ texture: TEXTURES.BACKGROUND_3 },
			{ texture: TEXTURES.BACKGROUND_4 },
		])

		this.title = this.add.text(0, 0, 'Somewhere behind the stars', {
			fontSize: '24pt',
			fontFamily: 'BoxyBoldFont'
		}).setDepth(50).setOrigin(0.5, 0.5)

		this.slogan = this.add.text(0, 0, "We don't choose our path, it chooses us").setOrigin(0.5, 0).setAlign('center')

		this.playButton = new SpriteButton(
			this,
			0,
			0,
			() => {
				this.scene.start(SCENES.SPACE);
			},
			TEXTURES.BUTTON_PLAY,
		).setScale(2).setAnimationOnPress(0, 3).setAnimationOnHover(3);


	}

	update(time: number, delta: number): void {
		super.update(time, delta)

		this.presentation.update(time, delta)

		this.title.setPosition(this.place.hCenter, this.place.vCenter)

		this.slogan.setPosition(this.place.hCenter, this.place.bottom * 3 / 4)
			.setSize(this.scale.width * 4 / 5, 50)

		this.playButton.setPosition(this.place.hCenter, this.place.bottom * 5/8)

	}
}