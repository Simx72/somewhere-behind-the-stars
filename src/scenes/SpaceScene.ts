/*
 * Somewhere behind the stars
 * 
 * Copyright (C) 2024  Simx72 <adventure5200@duck.com>
 * Copyright (c) 2022 pixel-fabian
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import { SCENES, TEXTURES } from "../constants";
import { Asteroid } from "../objects/Asteroid";
import { Bullet } from "../objects/Bullet";
import { Ship } from "../objects/Ship";
import { GameScene } from "./GameScene";

export class SpaceScene extends GameScene {
    constructor(config: Phaser.Types.Scenes.SettingsConfig) {
        super(Object.assign(
            {
                key: SCENES.SPACE,
                physics: {
                    default: 'arcade',
                    arcade: {
                        debug: false,
                    }
                }
            },
            config
        ));
    }


    minPixelSize = 700;

    create(): void {
        super.create()
        this.cameras.main.centerOn(0, 0);
        this.cameras.main.zoom = Math.min(this.scale.width, this.scale.height) / this.minPixelSize
        this.ships.add(new Ship(this, 0, 0, true, TEXTURES.SHIP_GREEN, 500, true))
        this.asteroids.add(new Asteroid(this, -500, 0));

        // overlaps
        this.physics.add.overlap(this.ships, this.asteroids);
        this.physics.add.overlap(this.bullets, this.asteroids);
        this.physics.world.on(Phaser.Physics.Arcade.Events.OVERLAP, (a: any, b: any) => this.overlapCheck(a, b))
    }

    overlapCheck<T extends Phaser.Types.Physics.Arcade.GameObjectWithBody, U extends Phaser.Types.Physics.Arcade.GameObjectWithBody>(obj1: T, obj2: U) {
        // console.log('omg a ship', obj1, obj2)
        if (obj1.body.onOverlap && obj2.body.onOverlap) {
            if (obj1.getData('type') === 'ship' && obj2.getData('type') === 'asteroid') {
                this.onShipTouchAsteroid(obj1 as any as Ship, obj2 as any as Asteroid)
            }
            if (obj1.getData('type') === 'bullet' && obj2.getData('type') === 'asteroid') {
                this.onBulletTouchAsteroid(obj1 as any as Bullet, obj2 as any as Asteroid)
            }
        }
    }

    onShipTouchAsteroid(ship: Ship, aster: Asteroid) {
        aster.destroy()
        ship.die()
    }

    onBulletTouchAsteroid(bullet: Bullet, aster: Asteroid) {
        aster.destroy()
        bullet.destroy()
    }

    onResize(): void {
        super.onResize()
        this.cameras.main.setViewport(0, 0, innerWidth, innerHeight)
        this.cameras.main.centerOn(0, 0)
        this.cameras.main.zoom = Math.min(this.scale.width, this.scale.height) / this.minPixelSize
    }

    update(time: number, delta: number): void {
        super.update(time, delta)
        this.ships.children.entries.forEach(ship => ship.update(delta));
    }

}