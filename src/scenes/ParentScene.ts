/*
 * Somewhere behind the stars
 * 
 * Copyright (C) 2024  Simx72 <adventure5200@duck.com>
 * Copyright (c) 2022 pixel-fabian
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

import { Background } from "../objects/Background";

export class ParentScene extends Phaser.Scene {

  //////////////////////////////////////////////////
  // LIFECYCLE (init, preload, create, update)    //
  //////////////////////////////////////////////////

  init(): void {
  }

  preload(): void { }

  create(): void {
    this.scale.on(Phaser.Scale.Events.RESIZE, () => this.onResize())
    this.background = new Background(this)
  }
  background!: Background;

  update(time: number, delta: number): void {
    this.background.update(time, delta)
  }

  onResize() {
    this.background.onResize()
  }

  //////////////////////////////////////////////////
  // Public methods                               //
  //////////////////////////////////////////////////

  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////
}