/*
 * Somewhere behind the stars
 * 
 * Copyright (C) 2024  Simx72 <adventure5200@duck.com>
 * Copyright (c) 2022 pixel-fabian
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import { ParentScene } from "./ParentScene";

export class UiScene extends ParentScene {

  //////////////////////////////////////////////////
  // LIFECYCLE (init, preload, create, update)    //
  //////////////////////////////////////////////////

  create(): void {
    super.create()
    this.onResize()
  }

  onResize(): void {
    super.onResize()
    this.place.right = innerWidth;
    this.place.bottom = innerHeight;
  }

  place = {
    left: 0,
    get hCenter () { return (this.right - this.left) /2 },
    right: innerWidth,
    top: 0,
    get vCenter () { return (this.bottom - this.top) /2 },
    bottom: innerHeight,
    get topLeft () {return new Phaser.Math.Vector2(this.left,this.top)},
    get topCenter () {return new Phaser.Math.Vector2(this.hCenter,this.top)},
    get topRight () {return new Phaser.Math.Vector2(this.right,this.top)},
    get centerLeft () {return new Phaser.Math.Vector2(this.left,this.vCenter)},
    get centerCenter () {return new Phaser.Math.Vector2(this.hCenter,this.vCenter)},
    get centerRight () {return new Phaser.Math.Vector2(this.right,this.vCenter)},
    get bottomLeft () {return new Phaser.Math.Vector2(this.left,this.bottom)},
    get bottomCenter () {return new Phaser.Math.Vector2(this.hCenter,this.bottom)},
    get bottomRight () {return new Phaser.Math.Vector2(this.right,this.bottom)},

  }


  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////

  
}
