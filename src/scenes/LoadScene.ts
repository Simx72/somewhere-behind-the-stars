/*
 * Somewhere behind the stars
 * 
 * Copyright (C) 2024  Simx72 <adventure5200@duck.com>
 * Copyright (c) 2022 pixel-fabian
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import { SCENES, TEXTURES } from '../constants';
import LoadingBar from '../objects/LoadingBar';

export class LoadScene extends Phaser.Scene {
  constructor() {
    super({
      key: SCENES.LOAD,
    });
  }

  //////////////////////////////////////////////////
  // LIFECYCLE (init, preload, create, update)    //
  //////////////////////////////////////////////////

  init(): void { }

  preload(): void {
    // add text
    this.add.text(360, 225, 'Loading...', {
      fontFamily: 'sans-serif',
      color: '#fff',
    });
    // create loading bar
    const loadingBar = new LoadingBar(this, 255, 255, 300, 40);
    this.load.on('progress', (percentage: number) => {
      loadingBar.fillBar(percentage);
    });
    // load all textures
    this.load.spritesheet(
      TEXTURES.BUTTON_PLAY,
      'assets/img/button_01_play.png',
      {
        frameWidth: 64,
        frameHeight: 32,
      },
    );
    this.load.spritesheet(
      TEXTURES.SHIP_GREEN,
      'assets/img/ship/green.png',
      {
        frameWidth: 43
      }
    )
    this.load.image(TEXTURES.BEAM_GREEN, 'assets/img/beam/green.png')
    this.load.image(TEXTURES.PARTICLES_GREEN, 'assets/img/particles/green.png')
    this.load.image(TEXTURES.ASTEROID_4, 'assets/exewin-spacegame/Other/meteor4.png')
    this.load.image(TEXTURES.BACKGROUND_1, 'assets/img/background/background_1.png')
    this.load.image(TEXTURES.BACKGROUND_2, 'assets/img/background/bg5.jpg')
    this.load.image(TEXTURES.BACKGROUND_3, 'assets/img/background/Parallax60.png')
    this.load.image(TEXTURES.BACKGROUND_4, 'assets/img/background/spacefield_a-000.png')
    // load all audio
  }

  animCreate(animkey: string, texture: string, start: number, end: number) {
    this.anims.create({
      key: texture + animkey,
      frames: this.anims.generateFrameNumbers(texture, {start, end}),
      repeat: -1
    })
  }

  create(): void {
    // create anims
    this.animCreate('idle', TEXTURES.SHIP_GREEN, 0, 0);
    this.animCreate('forward', TEXTURES.SHIP_GREEN, 1, 4);
    this.animCreate('backward', TEXTURES.SHIP_GREEN, 5, 8);
    this.animCreate('both', TEXTURES.SHIP_GREEN, 9, 12);
    // start
    this.scene.start(SCENES.START);
  }

  update(): void { }

  //////////////////////////////////////////////////
  // Private methods                              //
  //////////////////////////////////////////////////
}
