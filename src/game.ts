/*
 * Somewhere behind the stars
 * 
 * Copyright (C) 2024  Simx72 <adventure5200@duck.com>
 * Copyright (c) 2022 pixel-fabian
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import 'phaser';
import { LoadScene } from './scenes/LoadScene';
import { SpaceScene } from './scenes/SpaceScene';
import { StartScene } from './scenes/ui/StartScene';

const config: Phaser.Types.Core.GameConfig = {
  type: Phaser.AUTO, // WebGL if available
  title: 'Phaser Vite Template',
  width: window.innerWidth,
  height: window.innerHeight,
  parent: 'game',
  scene: [LoadScene, StartScene, SpaceScene],
  render: {
    pixelArt: true,
  }
};

window.onload = () => {
  // console.log(config);
  let game = new Phaser.Game(config);

  game.events.on("ready", () => {
    let willHide = document.querySelector<HTMLDivElement>('#willHide');
    if (willHide) {
      willHide.style.display = 'none';
    }
  })

  window.addEventListener('resize', _ => {
    game.scale.resize(innerWidth, innerHeight);
  })
};
