/*
 * Somewhere behind the stars
 * 
 * Copyright (C) 2024  Simx72 <adventure5200@duck.com>
 * Copyright (c) 2022 pixel-fabian
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


/* eslint-disable no-unused-vars */
export enum SCENES {
  LOAD = 'LoadScene',
  SPACE = 'SpaceScene',
  MENU = 'MenuScene', // T*T future
  START = 'start',
  Tutorial1 = 'tutorial-1'
}

export enum TEXTURES {
  BUTTON = 'button',
  BUTTON_PLAY = 'button_play',
  SHIP_GREEN = 'ship_green',
  BEAM_GREEN = 'beam_green',
  PARTICLES_GREEN = 'particles_green',
  ASTEROID_4 = 'asteroid_4',
  BACKGROUND_1 = 'background_1',
  BACKGROUND_2 = 'background_2',
  BACKGROUND_3 = 'background_3',
  BACKGROUND_4 = 'background_4',
}

export const COLORS = {
  BACKGROUND: new Phaser.Display.Color(18, 3, 48, 255),
  TEXT: new Phaser.Display.Color(255, 255, 255, 255),
  PRIMARY: new Phaser.Display.Color(242, 19, 183, 255),
};