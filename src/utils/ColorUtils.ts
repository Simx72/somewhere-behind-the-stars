/*
 * Somewhere behind the stars
 * 
 * Copyright (C) 2024  Simx72 <adventure5200@duck.com>
 * Copyright (c) 2022 pixel-fabian
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * Color converters
 */
export default class ColorUtils {
  /**
   * Converts Phaser Color class to HTML compatible color string, prefixed with #
   * @param color Phaser Color class
   */
  static colorToString(color: Phaser.Display.Color): string {
    return Phaser.Display.Color.RGBToString(
      color.red,
      color.green,
      color.blue,
      color.alpha,
      '#',
    );
  }

  /**
   * Converts Phaser Color class to rgba color string rgba(r,g,b,a)
   * @param color Phaser Color class
   */
  static colorToRGBAString(color: Phaser.Display.Color): string {
    return `rgba(${color.red},${color.green},${color.blue},1)`;
  }

  /**
   * Converts Phaser Color class to hex number
   * @param color Phaser Color class
   */
  static colorToHex(color: Phaser.Display.Color): number {
    const hexString = `0x${Phaser.Display.Color.ComponentToHex(
      color.red,
    )}${Phaser.Display.Color.ComponentToHex(
      color.green,
    )}${Phaser.Display.Color.ComponentToHex(color.blue)}`;

    return Number(hexString);
  }
}
