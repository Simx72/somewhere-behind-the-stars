/*
 * Somewhere behind the stars
 * 
 * Copyright (C) 2024  Simx72 <adventure5200@duck.com>
 * Copyright (c) 2022 pixel-fabian
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import { TEXTURES } from "../constants";
import { GameScene } from "../scenes/GameScene";
import { Bullet } from "./Bullet";

let startKeys = {
    controlForward: 'UP',
    controlBackward: 'DOWN',
    controlLeft: 'LEFT',
    controlRight: 'RIGHT',
    controlShoot: 'SPACE',
}

export enum ShipEvents {
    DID_DIE = 'did_die',
    DID_REVIVE = 'did_revive'

}

export class Ship extends Phaser.Physics.Arcade.Sprite {
    constructor(scene: GameScene, x: number = 0, y: number = 0, main: boolean, texture: TEXTURES = TEXTURES.SHIP_GREEN, maxVel = 500, controllable = false) {
        super(scene, x, y, texture);
        this.body = new Phaser.Physics.Arcade.Body(scene.physics.world, this);
        this.body.debugShowBody = true;
        scene.add.existing(this);
        scene.physics.world.add(this.body);
        // options
        this.body.setMaxSpeed(maxVel);
        this.setScale(3)
        this.setData({type: 'ship', lives: 3, main})
        this.body.onOverlap = true
        // colliders
        let redux = 0.1;
        this.setOrigin(0.5,0.5)
        this.body.setOffset(this.body.width * redux, this.body.height * redux)
        this.setCircle(this.body.width * 0.5 * (1-2*redux))
        // controls
        this.controllable = controllable;
        this.setAnim('idle')
    }

    private _alive = true;

    get alive(): boolean {
        return this._alive
    }

    getSpeedPercent() {
        return this.body.velocity.length() / this.body.maxSpeed;
    }

    getShipCenter() {
        let desplazamiento = new Phaser.Math.Vector2(0, 20).rotate(Phaser.Math.DegToRad(this.angle))
        return desplazamiento.add(this)
    }

    declare scene: GameScene;
    declare body: Phaser.Physics.Arcade.Body;

    impulso = 40; // lo que se demora en ir de 0 a 100
    impulsoReversa = 20; // lo que en frenar y dar reversa
    impulsoAngular = 10; // la velocidad pa girar

    _controllable = false;
    set controllable(controllable: boolean) {
        this._controllable = controllable;
        if (controllable) {
            this.controlForward = this.scene.input.keyboard?.addKey(startKeys.controlForward);
            this.controlBackward = this.scene.input.keyboard?.addKey(startKeys.controlBackward);
            this.controlLeft = this.scene.input.keyboard?.addKey(startKeys.controlLeft);
            this.controlRight = this.scene.input.keyboard?.addKey(startKeys.controlRight);
            this.controlShoot = this.scene.input.keyboard?.addKey(startKeys.controlShoot);
        } else {
            this.controlForward?.destroy();
            this.controlBackward?.destroy();
            this.controlLeft?.destroy();
            this.controlRight?.destroy();
            this.controlShoot?.destroy();
        }
    }
    get controllable(): boolean {
        return this._controllable;
    }
    setControllable(controllable = true) {
        this.controllable = controllable;
        return this;
    }
    controlForward?: Phaser.Input.Keyboard.Key;
    controlBackward?: Phaser.Input.Keyboard.Key;
    controlLeft?: Phaser.Input.Keyboard.Key;
    controlRight?: Phaser.Input.Keyboard.Key;
    controlShoot?: Phaser.Input.Keyboard.Key;


    update(delta: number, ..._args: any[]): void {
        // console.log(this.x, this.body.x, this.y, this.body.y);
        if (this.controllable && this.alive) {
            let del = this.controlForward?.isDown,
                tra = this.controlBackward?.isDown,
                izq = this.controlLeft?.isDown,
                der = this.controlRight?.isDown;

            /* control de propulsores */
            let accel = new Phaser.Math.Vector2(0, 0);
            // console.log(del, tra, izq, der);
            if (del) {
                accel.y -= this.impulso
            }
            if (tra) {
                accel.y += this.impulsoReversa
            }
            if (del && tra) {
                this.setAnim('both')
            } else if (del) {
                this.setAnim('forward')
            } else if (tra) {
                this.setAnim('backward')
            } else {
                this.setAnim('idle')
            }
            accel.rotate(this.rotation).scale(delta);
            this.body.acceleration.copy(accel);

            /* control del angulo */
            let rotationForce = 0;
            // console.log(del, tra, izq, der);
            if (izq) {
                rotationForce -= this.impulsoAngular
            }
            if (der) {
                rotationForce += this.impulsoAngular
            }
            this.angle += rotationForce * 0.3;


            /* disparadores */
            if (this.controlShoot) {
                if (this.scene.input.keyboard?.checkDown(this.controlShoot, 100)) {
                    // console.log('shoot')
                    this.disparar();
                }
            }
        }
    }

    setAnim(animkey: string) {
        if (this.anims.currentAnim?.key !== (this.texture.key + animkey)) {
            this.anims.startAnimation(this.texture.key + animkey)
        }
    }

    disparar() {
        new Bullet(this.scene, this.x, this.y, this.angle, TEXTURES.BEAM_GREEN);
    }

    die() {
        this._alive = false
        this.visible = false
        this.body.reset(0,0)
        this.angle = 0;
        this.body.onOverlap = false;
        this.emit(ShipEvents.DID_DIE)
        setTimeout(() => {
            this._revive()
        }, 1000);
    }
    
    _revive() {
        this._alive = true
        this.visible = true
        this.body.onOverlap = true;
        this.emit(ShipEvents.DID_REVIVE)
    }


}