/*
 * Somewhere behind the stars
 * 
 * Copyright (C) 2024  Simx72 <adventure5200@duck.com>
 * Copyright (c) 2022 pixel-fabian
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import { TEXTURES } from "../constants";
import { GameScene } from "../scenes/GameScene";

enum ASTEROID_SIZE {
    L = 500,
    M = 200,
    S = 100
}

export class Asteroid extends Phaser.Physics.Arcade.Sprite {
    constructor(scene: GameScene, x: number = 0, y:number = 0, texture: TEXTURES = TEXTURES.ASTEROID_4, size = ASTEROID_SIZE.S) {
        super(scene, x, y, texture);
        this.body = new Phaser.Physics.Arcade.Body(scene.physics.world, this);
        this.body.debugShowBody = true;
        scene.add.existing(this);
        scene.physics.world.add(this.body);
        // options
        this.body.onOverlap = true
        this.setData('type', 'asteroid')
        // collider
        this.displayWidth = size
        this.displayHeight = size
        let redux = 0.2;
        this.setOrigin(0.5,0.5)
        this.body.setOffset(this.body.width * redux, this.body.height * redux)
        this.setCircle(this.body.width * (1-2*redux) /2)
    }
    declare scene: GameScene;
    declare body: Phaser.Physics.Arcade.Body;
}