/*
 * Somewhere behind the stars
 * 
 * Copyright (C) 2024  Simx72 <adventure5200@duck.com>
 * Copyright (c) 2022 pixel-fabian
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import { TEXTURES } from "../constants";
import { ParentScene } from "../scenes/ParentScene";

export class Background extends Phaser.GameObjects.Image {
    constructor(scene: ParentScene) {
        super(scene, 0, 0, TEXTURES.BACKGROUND_1)
        this.scene = scene;
        scene.add.existing(this);
        // options
        this.setOrigin(0.5, 0.5)
        console.log(this.scene.cameras.main)
        this.onResize()
    }
    scene: ParentScene;
    adjustSizeToCamera(cam?: Phaser.Cameras.Scene2D.Camera) {
        let camera = cam ?? this.scene.cameras.main;

        let { x, y } = camera.midPoint
        this.setPosition(x, y)

        // set size
        console.log(camera.worldView.isEmpty())
        if (!camera.worldView.isEmpty()) {
            let viewport = camera.worldView;
            // viewport
            let scaleV = viewport.height / viewport.width;
            // background
            let scaleB = this.displayHeight / this.displayWidth
            // console.log(scaleV, scaleB)
            if (scaleB > scaleV) {
                // viewport mas aplastado que la imagen - se ajusta en función del ancho
                this.displayWidth = viewport.width;
                this.displayHeight = viewport.width * scaleB;
                // console.log('width set to', viewport.width)
            } else {
                // viewport mas alto que la imagen - se ajusta en función del alto
                this.displayHeight = viewport.height;
                this.displayWidth = viewport.height / scaleB;
                // console.log('height set to', viewport.height)
            }
        }
    }
    onResize() {
        if (this.scene) {
            this.adjustSizeToCamera()
        }
    }

    setTexture(key: string, frame?: string | number | undefined): this {
        super.setTexture(key, frame);
        this.onResize();
        return this;
    }

    setExactTexture(texture: ExactTexture<TEXTURES>): this {
        return this.setTexture(texture.texture, texture.frame);
    }

    update(_time: number, _delta: number): void {
        this.adjustSizeToCamera()
    }
}