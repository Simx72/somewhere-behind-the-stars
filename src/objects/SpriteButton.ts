/*
 * Somewhere behind the stars
 * 
 * Copyright (C) 2024  Simx72 <adventure5200@duck.com>
 * Copyright (c) 2022 pixel-fabian
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


export default class SpriteButton extends Phaser.GameObjects.Sprite {
  private onPress: Function;
  private ANIM_PRESS = 'animPress';
  private frameDefault = 0;

  constructor(
    scene: Phaser.Scene,
    x: number,
    y: number,
    onPress: () => void,
    texture: string | Phaser.Textures.Texture,
    frame?: number,
  ) {
    super(scene, x, y, texture, frame);
    scene.add.existing(this);

    this.onPress = onPress;
    this.setInteractive({ useHandCursor: true });
    this.on('pointerdown', () => {
      this.onPress();
    });

    if (frame) {
      this.frameDefault = frame;
    }
  }

  setAnimationOnPress(frameStart: number, frameEnd: number) {
    this.anims.create({
      key: this.ANIM_PRESS,
      frames: this.anims.generateFrameNumbers(this.texture.key, {
        start: frameStart,
        end: frameEnd,
      }),
      frameRate: 12,
      repeat: 0,
    });
    this.removeListener('pointerdown');
    this.on('pointerdown', () => {
      this.play(this.ANIM_PRESS);
    });
    this.on('animationcomplete', (animation: Phaser.Animations.Animation) => {
      switch (animation.key) {
        case this.ANIM_PRESS:
          this.onPress();
          break;
      }
    });

    return this;
  }

  setAnimationOnHover(
    frameHover: number,
    frameDefault: number = this.frameDefault,
  ) {
    this.on('pointerover', () => {
      this.setFrame(frameHover);
    });

    this.on('pointerout', () => {
      this.setFrame(frameDefault);
    });

    return this;
  }
}
