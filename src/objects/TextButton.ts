/*
 * Somewhere behind the stars
 * 
 * Copyright (C) 2024  Simx72 <adventure5200@duck.com>
 * Copyright (c) 2022 pixel-fabian
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


export default class TextButton extends Phaser.GameObjects.Text {
  private enabled = true;
  private hoverColor: string;
  private enabledColor: string;
  private disabledColor = '#bbbbbb';
  private onPress: Function;

  constructor(
    scene: Phaser.Scene,
    x: number,
    y: number,
    text: string,
    style: Phaser.Types.GameObjects.Text.TextStyle,
    hoverColor: string,
    onPress: Function,
  ) {
    const oStyle: Phaser.Types.GameObjects.Text.TextStyle = {
      fontFamily: 'Nunito',
      color: '#ffffff',
      fontSize: '16px',
      ...style,
    };
    super(scene, x, y, text, oStyle);
    scene.add.existing(this);

    this.enabledColor = oStyle.color as string;
    this.hoverColor = hoverColor;
    this.onPress = onPress;
    this.setEnabled(this.enabled);
  }

  setEnabled(bool: boolean) {
    this.enabled = bool;

    if (this.enabled) {
      this.setColor(this.enabledColor);
      this.setInteractive({ useHandCursor: true });
      this.on('pointerover', () => {
        this.setColor(this.hoverColor);
      });
      this.on('pointerout', () => {
        this.setColor(this.enabledColor);
      });
      this.on('pointerdown', () => {
        this.onPress();
      });
    } else {
      this.setColor(this.disabledColor);
      this.removeInteractive();
    }
  }
}
