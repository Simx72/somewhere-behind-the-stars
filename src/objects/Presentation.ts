/*
 * Somewhere behind the stars
 * 
 * Copyright (C) 2024  Simx72 <adventure5200@duck.com>
 * Copyright (c) 2022 pixel-fabian
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import { TEXTURES } from "../constants"
import {UiScene} from "../scenes/UiScene";


export enum PresentationTransition {
    NONE,
    DARK
}
export enum TransitionState {
    STARTING,
    ENDING,
    FINISHED
}


export class Presentation {
    constructor(scene: UiScene, textures: ExactTexture<TEXTURES>[]) {
        this.scene = scene;
        this.textures = textures;
        this.background.setExactTexture(textures[this._currentTexture])
        this.background.onResize()
    }

    private _currentTexture = 0;

    get background () {
        return this.scene.background;
    }

    scene: UiScene;
    textures: ExactTexture<TEXTURES>[];

    next() {
        this._currentTexture++;
        if (this._currentTexture >= this.textures.length) {
            this._currentTexture = 0;
        }
        this.background.setExactTexture(this.textures[this._currentTexture]);
    }

    last = 0;
    changeTime = 4000; // in milliseconds
    update(time: number, _delta: number) {
        if (time > this.last + this.changeTime) {
            this.next()
            this.last = time;
        }
    }
}