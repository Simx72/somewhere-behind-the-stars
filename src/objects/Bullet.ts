/*
 * Somewhere behind the stars
 * 
 * Copyright (C) 2024  Simx72 <adventure5200@duck.com>
 * Copyright (c) 2022 pixel-fabian
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


import { TEXTURES } from "../constants";
import { GameScene } from "../scenes/GameScene";

export class Bullet extends Phaser.Physics.Arcade.Sprite {
    constructor(scene: GameScene, x: number = 0, y:number = 0, angle: number = 10, texture: TEXTURES, vel: number = 2000, autodestroyTime = 3) {
        super(scene, x, y, texture);
        this.angle = angle;
        scene.add.existing(this);
        scene.physics.add.existing(this, false);
        scene.bullets.add(this)
        // options
        this.setData({ type: 'bullet' })
        // physics
        this.body.setSize(10, 10)
        this.body.onOverlap = true;
        let radAngle = Phaser.Math.DegToRad(angle);
        let newVel = new Phaser.Math.Vector2(0,-vel).rotate(radAngle);
        this.body.velocity.add(newVel);
        this.timeoutId = setTimeout(() => {
            this.destroy();
        }, autodestroyTime * 1000)
    }
    timeoutId: number;
    declare scene: GameScene;
    declare body: Phaser.Physics.Arcade.Body;
    destroy(fromScene?: boolean | undefined): void {
        clearTimeout(this.timeoutId)
        super.destroy(fromScene);
    }
}